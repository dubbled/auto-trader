import React from 'react'
import Request from 'superagent'
import {ResponsiveContainer, LineChart, XAxis, YAxis, Line } from 'recharts'

class TradeChart extends React.Component {
    constructor() {
        super();
        this.state = {
            trades: [],
        };
    }
    componentWillMount() {
        this.getData();
    }

    getData() {
        Request.get("http://localhost:8080/trades/btc-e?limit=10").end((err, res) => {
            if (err || !res.ok) {
                console.log(err);
            } else {
                var data = JSON.parse(res.text);
                console.log(data);
                this.setState({
                    trades : data
                });
            }
        });
    }
    render() {
        return (
        <ResponsiveContainer height={300} width="100%">
            <LineChart data={this.state.trades}>
                <XAxis dataKey="Time"/>
                <YAxis domain={['dataMin', 'dataMax']}/>
                <Line type="monotone" dataKey="Price" stroke="#1e1e1e" />
            </LineChart>
        </ResponsiveContainer>)
    }
}

export default TradeChart;
